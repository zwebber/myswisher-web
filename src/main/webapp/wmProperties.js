var _WM_APP_PROPERTIES = {
  "activeTheme" : "material",
  "defaultLanguage" : "en",
  "displayName" : "MySwisherWeb",
  "homePage" : "Main",
  "name" : "MySwisherWeb",
  "platformType" : "WEB",
  "supportedLanguages" : "en",
  "type" : "APPLICATION",
  "version" : "1.0"
};